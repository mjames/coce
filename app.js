// Get coce configuration.
var coce = require('./coce');

// Create the http and https servers.

var fs = require('fs');
var http = require('http');
var https = require('https');
var express = require('express');
var app = express();

var httpServer = http.createServer(app);
httpServer.listen(coce.config.port);

if (typeof coce.config.sslport !== 'undefined') {
    var privateKey  = fs.readFileSync(coce.config.key, 'utf8');
    var certificate = fs.readFileSync(coce.config.cert, 'utf8');
    var credentials = {key: privateKey, cert: certificate};
    var httpsServer = https.createServer(credentials, app);
    httpsServer.listen(coce.config.sslport);
}

app.get('/', function(req, res) {
    res.send('Welcome to coce');
});

var isbnRE = /([0-9X]{10,13})/;

app.get('/cover', function(req, res) {
    var ids = req.query.id;
    if (ids === undefined || ids.length < 8) {
	var fail = {};
	fail.error = "ID parameter is missing";
        res.send(fail);
        return;
    }
    ids = ids.split(',');
    var idsNew = [];
    for (id in ids) {
        var re;
        if (re = isbnRE.exec(ids[id])) {
            idsNew.push(re[1]);
        } else {
            //console.log('non: '+ids[id]);
        }
    }
    ids = idsNew;
    if (ids.length === 0) { 
	var fail = {};
	fail.error = "Bad id parameter";
	res.send(fail);
	return;
    }
    var providers = req.query.provider;
    providers = providers == undefined ? coce.config.providers : providers.split(',');
    var callback = req.query.callback;

    var fetcher = new coce.CoceFetcher();
    fetcher.fetch(ids, providers, function(url) {
        if ( req.query.all === undefined ) {
            // Not &all param: URL are picked up by provider priority order
            var ret = {};
            for (var id in url)
                for (var j=0, provider; provider = providers[j]; j++) {
                    var u = url[id][provider];
                    if ( u !== undefined ) { ret[id] = u; break; }
                }
            url = ret;
        }
        if (callback) {
            res.contentType("application/javascript");
            url = callback + '(' + JSON.stringify(url) + ')'
        } else {
            res.contentType("application/json");
        }
        res.send(url);
    });
});

